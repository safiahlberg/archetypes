# Archetypes #

This is an archetype distribution repository for maven archetypes.

# Usage #

To create a project with an archetype, you can run Maven as such:

```
#!bash

mvn archetype:generate \
-DarchetypeCatalog=https://bitbucket.org/safiahlberg/archetypes/raw/releases/archetype-catalog.xml
```